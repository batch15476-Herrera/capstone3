//React bootstrap components
import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import Slider from "./Slider";

export default function Banner() {
  return (
    <Row>
      <Slider />
      <Col className="p-5">
        <h1 className="mb-3">Hello future Rockstar!</h1>
        <p className="mb-3">
          Learn how to play like a professional in just 2 weeks.
        </p>
        <Button variant="dark" as={Link} to="/courses">
          Enroll now!
        </Button>
      </Col>
    </Row>
  );
}
