import React from "react";
import Slider1 from "./images/slider1.jpg";
import Slider2 from "./images/slider2.jpg";
import Slider3 from "./images/slider3.jpg";
import Carousel from "react-bootstrap/Carousel";

function Slider() {
  return (
    <Carousel variant="dark">
      <Carousel.Item>
        <img className="d-block w-100" src={Slider1} alt="First slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={Slider2} alt="Second slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={Slider3} alt="Third slide" />
      </Carousel.Item>
    </Carousel>
  );
}

export default Slider;
