import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row className="container-fluid d-flex justify-content-center">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3 h-100">
          <Card.Body>
            <Card.Title>
              <h2>Learn Anytime Anywhere</h2>
            </Card.Title>

            <Card.Text>
              Our program is very versitile when it comes to learning. We
              understand that every student has their own learning structures,
              and we give our students the option to learn from their lovely
              homes or visit our headquarters to give them that physical
              interaction when it comes to teaching students.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3 h-100">
          <Card.Body>
            <Card.Title>
              <h2>About Rock Mojo LLC</h2>
            </Card.Title>

            <Card.Text>
              We are a collection of artists who want to spread our teachings
              around the world for everyone! We believe that music changes lives
              of the people regardless of culture, gender, genre or status. Our
              company is founded by the one and only Polyphia. Come check us
              out! We'd be delighted to have you.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3 h-100">
          <Card.Body>
            <Card.Title>
              <h2>Be Part of Our Community</h2>
            </Card.Title>

            <Card.Text>
              Discover people with the same music taste, core values and
              dedication for art. Our community will be very pleased to have you
              as long as you adhere to our rules and regulations, mainly be nice
              to each other and respect is all we require from all our members.
              Hop on board!
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
